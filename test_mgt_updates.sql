/************************************************/
/********* TESTING - MGT UPDATES ****************/
/* Test results are for code in branch: https://gitlab.com/meettally/financial-analytics/onboarding/-/blob/ms_mgt_migration_off_consumer/Master%20Growth%20Development/master_growth.sql?ref_type=heads */
/************************************************/

--1. debt_trck_pop --> LOOKS GOOD
create temporary table og_debt_trck_pop AS (
SELECT * FROM (
SELECT a.id as consumer_id, consumer_key, created_on, activated_on, b.eligible as first_eligibility, b.as_of as application_dt,
       ROW_NUMBER() OVER (PARTITION BY b.consumer_id ORDER BY b.as_of) as Rw
FROM bankcore_pii.consumer a
LEFT JOIN bankcore_pii.underwriting_history b
ON a.id = b.consumer_id) AS dtp
WHERE Rw = 1 AND first_eligibility IS FALSE AND activated_on IS NOT NULL);


create temporary table new_debt_trck_pop AS (
SELECT * FROM (
SELECT a.loc_account_key, created_on, activated_on, b.eligible as first_eligibility, b.as_of as application_dt,
       ROW_NUMBER() OVER (PARTITION BY b.loc_account_key ORDER BY b.as_of) as Rw
FROM bc_pii.loc_account a
LEFT JOIN bc_pii.underwriting_history b
ON a.loc_account_key = b.loc_account_key) AS dtp
WHERE Rw = 1 AND first_eligibility IS FALSE AND activated_on IS NOT NULL);

select count(*), count(distinct consumer_key) from og_debt_trck_pop; --145056,145056
select count(*), count(distinct loc_account_key) from new_debt_trck_pop; --145056,145056

--2.dt_grad_pop --> LOOKS GOOD
create temporary table og_dt_grad_pop AS (
    SELECT consumer_id, MIN(as_of AT TIME ZONE 'America/Los_Angeles'::DATE) as grad_offer_dt, eligible as grad_eligibility, is_loan_offer, decision_code, executed_on as dt_accept_dt
    FROM bankcore_pii.underwriting_history
    WHERE grad_eligibility IS TRUE and is_loan_offer IS TRUE AND decision_code = 'SPDC_313'
    GROUP BY 1,3,4,5,6);


create temporary table new_dt_grad_pop AS (
    SELECT loc_account_key, MIN(as_of AT TIME ZONE 'America/Los_Angeles'::DATE) as grad_offer_dt, eligible as grad_eligibility, is_loan_offer, decision_code, executed_on as dt_accept_dt
    FROM bc_pii.underwriting_history
    WHERE grad_eligibility IS TRUE and is_loan_offer IS TRUE AND decision_code = 'SPDC_313'
    GROUP BY 1,3,4,5,6);

select count(*), count(distinct consumer_id) from og_dt_grad_pop; --829, 829
select count(*), count(distinct loc_account_key) from new_dt_grad_pop; --829, 829

--3. debt_tracker_pop --> LOOKS GOOD
create temporary table og_debt_tracker_pop AS (
SELECT dtp.consumer_id, dtp.consumer_key, dtp.created_on, dtp.activated_on, dtp.application_dt, dtp.first_eligibility, dgp.grad_eligibility, dgp.grad_offer_dt, dgp.decision_code, dgp.dt_accept_dt, dgp.is_loan_offer
FROM og_debt_trck_pop dtp
LEFT JOIN og_dt_grad_pop dgp
ON dtp.consumer_id = dgp.consumer_id
WHERE first_eligibility IS FALSE AND activated_on IS NOT NULL);

create temporary table new_debt_tracker_pop AS (
SELECT dtp.loc_account_key, dtp.created_on, dtp.activated_on, dtp.application_dt, dtp.first_eligibility, dgp.grad_eligibility, dgp.grad_offer_dt, dgp.decision_code, dgp.dt_accept_dt, dgp.is_loan_offer
FROM new_debt_trck_pop dtp
LEFT JOIN new_dt_grad_pop dgp
ON dtp.loc_account_key = dgp.loc_account_key
WHERE first_eligibility IS FALSE AND activated_on IS NOT NULL);

select count(*), count(distinct consumer_id), count(grad_eligibility) from og_debt_tracker_pop; --145056,145056, 829
select count(*), count(distinct loc_account_key), count(grad_eligibility) from new_debt_tracker_pop; -- 145056,145056

--4. all_users (changes except to marketing.prequal_affiliate_funnel and temp.kpi_borrowers_settled)

DROP TABLE IF EXISTS og_all_users;
CREATE TEMPORARY TABLE og_all_users AS (
    SELECT DISTINCT
        a.user_key,
        a.user_id,
        b.id as consumer_id,
        b.consumer_key,
        a.attribution_id, --based on email address; not a primary key
        a.user_name AS email,
        CASE
            WHEN prequal_api.consumer_key IS NOT NULL AND prequal_api.pw_reset_dt IS NOT NULL THEN 'prequal'
            else 'not_prequal'
            END   AS Account_Type, --this is a new field that allows MGT users to filter accounts based on whether or not they were acquired through prequal api

        CASE WHEN prequal_api.consumer_key IS NULL AND debt_tracker_pop.consumer_key IS NULL THEN (a.created_on AT TIME ZONE 'America/Los_Angeles')::DATE --nonprequal, non-debt tracker
        WHEN prequal_api.consumer_key IS NOT NULL AND prequal_api.pw_reset_dt IS NOT NULL THEN (prequal_api.created_at AT TIME ZONE 'America/Los_Angeles')::DATE
        WHEN debt_tracker_pop.consumer_key IS NOT NULL THEN (a.created_on AT TIME ZONE 'America/Los_Angeles')::DATE --debt tracker
            END AS created_dt,

        CASE WHEN prequal_api.consumer_key IS NULL AND debt_tracker_pop.consumer_key IS NULL THEN (first_dec.as_of AT TIME ZONE 'America/Los_Angeles')::DATE --nonprequal, non-debt tracker
        WHEN prequal_api.consumer_key IS NOT NULL AND prequal_api.pw_reset_dt IS NOT NULL THEN (prequal_api.application_dt AT TIME ZONE 'America/Los_Angeles')::DATE
        WHEN debt_tracker_pop.consumer_key IS NOT NULL THEN (first_dec.as_of AT TIME ZONE 'America/Los_Angeles')::DATE --debt tracker
            END AS application_dt,

        CASE WHEN first_dec.eligible AND prequal_api.consumer_key IS NULL AND debt_tracker_pop.consumer_key IS NULL THEN (first_dec.as_of AT TIME ZONE 'America/Los_Angeles')::DATE
        WHEN prequal_api.consumer_key IS NOT NULL AND prequal_api.pw_reset_dt IS NOT NULL THEN (prequal_api.eligible_dt AT TIME ZONE 'America/Los_Angeles')::DATE
        WHEN debt_tracker_pop.consumer_key IS NOT NULL AND debt_tracker_pop.first_eligibility IS FALSE AND debt_tracker_pop.grad_eligibility IS TRUE AND debt_tracker_pop.grad_offer_dt IS NOT NULL AND debt_tracker_pop.is_loan_offer IS TRUE AND debt_tracker_pop.decision_code = 'SPDC_313' THEN (grad_offer_dt AT TIME ZONE 'America/Los_Angeles')::DATE
            END AS eligible_dt,

        CASE
        WHEN prequal_api.consumer_key IS NULL  THEN card_input.card_input_dt
        WHEN prequal_api.consumer_key IS NOT NULL AND prequal_api.pw_reset_dt IS NOT NULL THEN prequal_api.card_input_dt
            END AS card_input_dt,

        CASE WHEN prequal_api.consumer_key IS NULL AND last_dec.eligible AND last_dec.is_loan_offer THEN (last_dec.as_of AT TIME ZONE 'America/Los_Angeles')::DATE
        WHEN prequal_api.consumer_key IS NOT NULL AND prequal_api.pw_reset_dt IS NOT NULL THEN (prequal_api.seen_offer_dt AT TIME ZONE 'America/Los_Angeles')::DATE
            END   AS offer_dt, --for prequal accounts, offer dt populated is actually the date when the user sees the offer while on the Tally site

        CASE WHEN prequal_api.consumer_key IS NULL AND last_dec.eligible AND last_dec.is_loan_offer THEN (last_dec.executed_on AT TIME ZONE 'America/Los_Angeles')::DATE
        WHEN prequal_api.consumer_key IS NOT NULL AND prequal_api.pw_reset_dt IS NOT NULL AND prequal_api.accept_dt IS NOT NULL THEN (prequal_api.accept_dt AT TIME ZONE 'America/Los_Angeles')::DATE
        END               AS accept_dt,

        CASE WHEN prequal_api.consumer_key IS NULL THEN (b.activated_on AT TIME ZONE 'America/Los_Angeles')::DATE
        WHEN prequal_api.consumer_key IS NOT NULL AND prequal_api.pw_reset_dt IS NOT NULL AND prequal_api.accept_dt IS NOT NULL AND prequal_api.activated_dt IS NOT NULL THEN (prequal_api.activated_dt AT TIME ZONE 'America/Los_Angeles')::DATE
        END                     AS activated_dt,

        CASE WHEN prequal_api.consumer_key IS NULL THEN (b.kyc_approved AT TIME ZONE 'America/Los_Angeles')::DATE
        WHEN prequal_api.consumer_key IS NOT NULL AND prequal_api.pw_reset_dt IS NOT NULL AND prequal_api.kyc_dt IS NOT NULL THEN (prequal_api.kyc_dt AT TIME ZONE 'America/Los_Angeles')::DATE
        END                     AS kyc_dt,

        CASE WHEN prequal_api.consumer_key IS NULL  THEN borr.borrower_dt
        WHEN prequal_api.consumer_key IS NOT NULL THEN prequal_api.borrower_dt
        END                AS borrower_dt,

        FIRST_VALUE(last_dec.decision_code)
            OVER (PARTITION BY a.user_key
                ORDER BY borr.borrower_dt NULLS LAST, kyc_dt NULLS LAST, activated_dt NULLS LAST, accept_dt NULLS LAST, offer_dt NULLS LAST, eligible_dt NULLS LAST, application_dt NULLS LAST
                ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS decision_code,
        FIRST_VALUE(
            CASE WHEN do_agreement.consumer_id IS NOT NULL THEN 'Discount Offer'
                WHEN last_dec.eligible AND last_dec.is_loan_offer AND last_dec.executed_on IS NOT NULL THEN 'Legacy'
                END)
            OVER (PARTITION BY a.user_key
                ORDER BY borr.borrower_dt NULLS LAST, kyc_dt NULLS LAST, activated_dt NULLS LAST, accept_dt NULLS LAST
                ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS pricing_version,
        FIRST_VALUE(CASE WHEN first_dec.eligible THEN last_dec.originator END IGNORE NULLS)
            OVER (PARTITION BY a.user_key
                ORDER BY borr.borrower_dt NULLS LAST, kyc_dt NULLS LAST, activated_dt NULLS LAST, accept_dt NULLS LAST, offer_dt NULLS LAST, eligible_dt NULLS LAST
                ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS originator,
        FIRST_VALUE(user_platform.platform IGNORE NULLS)
            OVER (PARTITION BY a.user_key
                ORDER BY created_dt NULLS LAST
                ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS all_users_platform,
        FIRST_VALUE(CASE WHEN last_dec.fico < 600 then 'A:<600'
                        when last_dec.fico < 620 then 'B:<620'
                        when last_dec.fico < 640 then 'C:<640'
                        when last_dec.fico < 660 then 'D:<660'
                        when last_dec.fico < 700 then 'E:<700'
                        when last_dec.fico >= 700 then 'F:>=700'
                        END IGNORE NULLS)
            OVER (PARTITION BY a.user_key
                ORDER BY created_dt NULLS LAST
                ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS fico_bucket,
        FIRST_VALUE(CASE WHEN ob_card_summ.ob_revolver_ct <= 0 THEN 'A:<=0'
                        WHEN ob_card_summ.ob_revolver_ct <= 1 THEN 'B:<=1'
                        WHEN ob_card_summ.ob_revolver_ct <= 2 THEN 'C:<=2'
                        WHEN ob_card_summ.ob_revolver_ct > 2 THEN 'D:>2'
                        END IGNORE NULLS)
            OVER (PARTITION BY a.user_key
                ORDER BY created_dt NULLS LAST
                ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS revolving_card_bucket,
        FIRST_VALUE(CASE WHEN last_dec.revolving_balance <= 3000 THEN 'A:<=3K'
                        WHEN last_dec.revolving_balance <= 6000 THEN 'B:<=6K'
                        WHEN last_dec.revolving_balance <= 10000 THEN 'C:<=10K'
                        WHEN last_dec.revolving_balance <= 20000 THEN 'D:<=20K'
                        WHEN last_dec.revolving_balance > 20000 THEN 'E:>20K'
                        END IGNORE NULLS)
            OVER (PARTITION BY a.user_key
                ORDER BY created_dt NULLS LAST
                ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS revolving_balance_bucket
    --joins/table references begin
    FROM
        user_management_pii.user_account a --base account table; contains info about a customer
        LEFT JOIN bankcore_pii.consumer b --base consumer table
            ON a.user_key=b.consumer_key
        LEFT JOIN (
            SELECT DISTINCT
                consumer_id,
                eligible,
                as_of,
                RANK() OVER (PARTITION BY consumer_id ORDER BY id) AS dec_rnk
            FROM bankcore_pii.underwriting_history
        ) first_dec --sometimes a person will have two "decisions" during their application; first for eligibility and the latter for pricing
            ON b.id = first_dec.consumer_id
            AND first_dec.dec_rnk = 1
        LEFT JOIN (
            SELECT DISTINCT
                uh.consumer_id,
                as_of,
                executed_on,
                eligible,
                is_loan_offer,
                decision_code,
                originator,
                CASE WHEN cdd.fico9_score BETWEEN 300 AND 850 THEN cdd.fico9_score END AS fico, --valid FICOs
                cdd.revolving_balance,
                RANK() OVER (PARTITION BY uh.consumer_id ORDER BY executed_on DESC NULLS LAST, uh.id DESC) AS dec_rnk
            FROM bankcore_pii.underwriting_history uh
            LEFT JOIN bankcore_pii.consumer_decisioning_detail cdd
                ON uh.consumer_decisioning_detail_id = cdd.id
        ) last_dec --sometimes a person will have two "decisions" during their application
            ON b.id = last_dec.consumer_id
            AND last_dec.dec_rnk = 1
        LEFT JOIN (
            SELECT DISTINCT
                uwh.consumer_id,
                as_of,
                executed_on,
                eligible,
                is_loan_offer,
                decision_code,
                originator,
                CASE WHEN dec_det.fico9_score BETWEEN 300 AND 850 THEN dec_det.fico9_score END AS fico, --valid FICOs
                dec_det.revolving_balance,
                RANK() OVER (PARTITION BY uwh.consumer_id ORDER BY executed_on DESC NULLS LAST, uwh.id DESC) AS RnW
            FROM bankcore_pii.underwriting_history uwh
            LEFT JOIN bankcore_pii.consumer_decisioning_detail dec_det
                ON uwh.consumer_decisioning_detail_id = dec_det.id
        ) debt_track_dec
        ON b.id = debt_track_dec.consumer_id
        AND debt_track_dec.RnW = 2
        LEFT JOIN (
            SELECT *
            FROM temp.kpi_borrowers_settled --this is an analyst-managed table that tracks when people become borrowers
            WHERE month_num_asc = 0 --this table tracks borrower status monthly, 0 is the first month they appear on record
        ) borr --table constructed to identify who is a borrower/not
            ON b.id = borr.consumer_id
        LEFT JOIN (
            SELECT DISTINCT consumer_id,
                    MIN(a.created_on AT TIME ZONE 'America/Los_Angeles'::DATE) as card_input_dt
        FROM bankcore_pii.card_account a -- updated to pull from a bankcore table instead of segment
        WHERE a.customer_approval_status <> 'INIT'
        GROUP BY 1
        ) card_input --table that uses Segment screens to identify that someone has successfully added at least one cards
            ON card_input.consumer_id = b.id
        LEFT JOIN (
            SELECT DISTINCT
                user_id,
                FIRST_VALUE(platform) OVER (PARTITION BY user_id ORDER BY created_on ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS platform
            FROM user_management_pii.signup_user_agent_detail -- this table is a transformation of segment data. Probably best to go straight to the source
        ) user_platform --used to identify ios, android, or web
            ON user_platform.user_id = a.user_id
        LEFT JOIN (
            SELECT DISTINCT consumer_id FROM bankcore_pii.loan_discount_agreement
        ) do_agreement --if a consumer_id is in this table, they're a discount offer customer
            ON do_agreement.consumer_id = b.id
        LEFT JOIN (
            SELECT DISTINCT
                consumer_key,
                COUNT(DISTINCT CASE WHEN ob_pmt_strategy = 'REVOLVER' THEN card_id END) AS ob_revolver_ct
            FROM (
                SELECT DISTINCT
                    cons.consumer_key,
                    card.id AS card_id,
                    card.organization_id,
                    card.card_number,
                    cash.init_pmt_strategy,
                    card.initial_payment_strategy,
                    CASE WHEN cash.init_pmt_strategy = 'LINK_FAILED'
                            OR cash.first_summ < cons.activated_on
                            THEN cash.init_pmt_strategy
                        ELSE card.initial_payment_strategy
                        END AS ob_pmt_strategy
                FROM
                    bankcore_pii.consumer AS cons --list of consumer accounts
                    LEFT JOIN bankcore_pii.card_account AS card --list of all cards added by consumers
                        ON card.consumer_id = cons.id
                        AND (
                            card.created_on < cons.activated_on --customers can add cards later in their lifecycle, so we're limiting it here to details of cards which they added during onboarding
                            OR cons.activated_on IS NULL
                            ) --still collect details for customers who add cards but don't activate
                    LEFT JOIN (
                        SELECT DISTINCT
                            card_id,
                            FIRST_VALUE(payment_strategy) OVER (PARTITION BY card_id ORDER BY created_on ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS init_pmt_strategy,
                            FIRST_VALUE(created_on) OVER (PARTITION BY card_id ORDER BY created_on ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS first_summ
                        FROM bankcore_pii.card_account_summary_history --contains history of cards added and their statuses over time
                    ) AS cash
                        ON cash.card_id = card.id
            ) AS ob_cards
            GROUP BY 1
        ) AS ob_card_summ --summary of cards added during onboarding experience
            ON ob_card_summ.consumer_key = b.consumer_key
        LEFT JOIN ( --Accounts created through affiliate API. These are pulled using the tables in the affiliates schema
            SELECT DISTINCT
                consumer_key,
                consumer_id,
                affiliate_vendor,
                created_at,
                application_dt,
                eligible_dt,
                card_input_dt,
                pw_reset_dt,
                seen_offer_dt,
                offer_dt,
                accept_dt,
                kyc_dt,
                activated_dt,
                borrower_dt
            FROM
                marketing.prequal_affiliate_funnel
        ) AS prequal_api
            ON a.user_key = prequal_api.consumer_key
	LEFT JOIN (
	    SELECT consumer_id,
	           consumer_key,
	           created_on,
	           activated_on,
	           first_eligibility,
	           grad_eligibility,
	           grad_offer_dt,
	           decision_code,
	           dt_accept_dt,
	           is_loan_offer
	    FROM og_debt_tracker_pop) debt_tracker_pop
	ON b.id = debt_tracker_pop.consumer_id
);


DROP TABLE IF EXISTS new_all_users;
CREATE TEMPORARY TABLE new_all_users AS (
    SELECT DISTINCT
        a.user_key,
        a.user_id,
        b.loc_account_key,
        a.attribution_id, --based on email address; not a primary key
        a.user_name AS email,
        CASE
            WHEN prequal_api.consumer_key IS NOT NULL AND prequal_api.pw_reset_dt IS NOT NULL THEN 'prequal'
            else 'not_prequal'
            END   AS Account_Type, --this is a new field that allows MGT users to filter accounts based on whether or not they were acquired through prequal api

        CASE WHEN prequal_api.consumer_key IS NULL AND debt_tracker_pop.loc_account_key IS NULL THEN (a.created_on AT TIME ZONE 'America/Los_Angeles')::DATE --nonprequal, non-debt tracker
        WHEN prequal_api.consumer_key IS NOT NULL AND prequal_api.pw_reset_dt IS NOT NULL THEN (prequal_api.created_at AT TIME ZONE 'America/Los_Angeles')::DATE
        WHEN debt_tracker_pop.loc_account_key IS NOT NULL THEN (a.created_on AT TIME ZONE 'America/Los_Angeles')::DATE --debt tracker
            END AS created_dt,

        CASE WHEN prequal_api.consumer_key IS NULL AND debt_tracker_pop.loc_account_key IS NULL THEN (first_dec.as_of AT TIME ZONE 'America/Los_Angeles')::DATE --nonprequal, non-debt tracker
        WHEN prequal_api.consumer_key IS NOT NULL AND prequal_api.pw_reset_dt IS NOT NULL THEN (prequal_api.application_dt AT TIME ZONE 'America/Los_Angeles')::DATE
        WHEN debt_tracker_pop.loc_account_key IS NOT NULL THEN (first_dec.as_of AT TIME ZONE 'America/Los_Angeles')::DATE --debt tracker
            END AS application_dt,

        CASE WHEN first_dec.eligible AND prequal_api.consumer_key IS NULL AND debt_tracker_pop.loc_account_key IS NULL THEN (first_dec.as_of AT TIME ZONE 'America/Los_Angeles')::DATE
        WHEN prequal_api.consumer_key IS NOT NULL AND prequal_api.pw_reset_dt IS NOT NULL THEN (prequal_api.eligible_dt AT TIME ZONE 'America/Los_Angeles')::DATE
        WHEN debt_tracker_pop.loc_account_key IS NOT NULL AND debt_tracker_pop.first_eligibility IS FALSE AND debt_tracker_pop.grad_eligibility IS TRUE AND debt_tracker_pop.grad_offer_dt IS NOT NULL AND debt_tracker_pop.is_loan_offer IS TRUE AND debt_tracker_pop.decision_code = 'SPDC_313' THEN (grad_offer_dt AT TIME ZONE 'America/Los_Angeles')::DATE
            END AS eligible_dt,

        CASE
        WHEN prequal_api.consumer_key IS NULL  THEN card_input.card_input_dt
        WHEN prequal_api.consumer_key IS NOT NULL AND prequal_api.pw_reset_dt IS NOT NULL THEN prequal_api.card_input_dt
            END AS card_input_dt,

        CASE WHEN prequal_api.consumer_key IS NULL AND last_dec.eligible AND last_dec.is_loan_offer THEN (last_dec.as_of AT TIME ZONE 'America/Los_Angeles')::DATE
        WHEN prequal_api.consumer_key IS NOT NULL AND prequal_api.pw_reset_dt IS NOT NULL THEN (prequal_api.seen_offer_dt AT TIME ZONE 'America/Los_Angeles')::DATE
            END   AS offer_dt, --for prequal accounts, offer dt populated is actually the date when the user sees the offer while on the Tally site

        CASE WHEN prequal_api.consumer_key IS NULL AND last_dec.eligible AND last_dec.is_loan_offer THEN (last_dec.executed_on AT TIME ZONE 'America/Los_Angeles')::DATE
        WHEN prequal_api.consumer_key IS NOT NULL AND prequal_api.pw_reset_dt IS NOT NULL AND prequal_api.accept_dt IS NOT NULL THEN (prequal_api.accept_dt AT TIME ZONE 'America/Los_Angeles')::DATE
        END               AS accept_dt,

        CASE WHEN prequal_api.consumer_key IS NULL THEN (b.activated_on AT TIME ZONE 'America/Los_Angeles')::DATE
        WHEN prequal_api.consumer_key IS NOT NULL AND prequal_api.pw_reset_dt IS NOT NULL AND prequal_api.accept_dt IS NOT NULL AND prequal_api.activated_dt IS NOT NULL THEN (prequal_api.activated_dt AT TIME ZONE 'America/Los_Angeles')::DATE
        END                     AS activated_dt,

        CASE WHEN prequal_api.consumer_key IS NULL THEN (b.kyc_approved AT TIME ZONE 'America/Los_Angeles')::DATE
        WHEN prequal_api.consumer_key IS NOT NULL AND prequal_api.pw_reset_dt IS NOT NULL AND prequal_api.kyc_dt IS NOT NULL THEN (prequal_api.kyc_dt AT TIME ZONE 'America/Los_Angeles')::DATE
        END                     AS kyc_dt,

        CASE WHEN prequal_api.consumer_key IS NULL  THEN borr.borrower_dt
        WHEN prequal_api.consumer_key IS NOT NULL THEN prequal_api.borrower_dt
        END                AS borrower_dt,

        FIRST_VALUE(last_dec.decision_code)
            OVER (PARTITION BY a.user_key
                ORDER BY borr.borrower_dt NULLS LAST, kyc_dt NULLS LAST, activated_dt NULLS LAST, accept_dt NULLS LAST, offer_dt NULLS LAST, eligible_dt NULLS LAST, application_dt NULLS LAST
                ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS decision_code,
        FIRST_VALUE(
            CASE WHEN do_agreement.loc_account_key IS NOT NULL THEN 'Discount Offer'
                WHEN last_dec.eligible AND last_dec.is_loan_offer AND last_dec.executed_on IS NOT NULL THEN 'Legacy'
                END)
            OVER (PARTITION BY a.user_key
                ORDER BY borr.borrower_dt NULLS LAST, kyc_dt NULLS LAST, activated_dt NULLS LAST, accept_dt NULLS LAST
                ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS pricing_version,
        FIRST_VALUE(CASE WHEN first_dec.eligible THEN last_dec.originator END IGNORE NULLS)
            OVER (PARTITION BY a.user_key
                ORDER BY borr.borrower_dt NULLS LAST, kyc_dt NULLS LAST, activated_dt NULLS LAST, accept_dt NULLS LAST, offer_dt NULLS LAST, eligible_dt NULLS LAST
                ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS originator,
        FIRST_VALUE(user_platform.platform IGNORE NULLS)
            OVER (PARTITION BY a.user_key
                ORDER BY created_dt NULLS LAST
                ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS all_users_platform,
        FIRST_VALUE(CASE WHEN last_dec.fico < 600 then 'A:<600'
                        when last_dec.fico < 620 then 'B:<620'
                        when last_dec.fico < 640 then 'C:<640'
                        when last_dec.fico < 660 then 'D:<660'
                        when last_dec.fico < 700 then 'E:<700'
                        when last_dec.fico >= 700 then 'F:>=700'
                        END IGNORE NULLS)
            OVER (PARTITION BY a.user_key
                ORDER BY created_dt NULLS LAST
                ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS fico_bucket,
        FIRST_VALUE(CASE WHEN ob_card_summ.ob_revolver_ct <= 0 THEN 'A:<=0'
                        WHEN ob_card_summ.ob_revolver_ct <= 1 THEN 'B:<=1'
                        WHEN ob_card_summ.ob_revolver_ct <= 2 THEN 'C:<=2'
                        WHEN ob_card_summ.ob_revolver_ct > 2 THEN 'D:>2'
                        END IGNORE NULLS)
            OVER (PARTITION BY a.user_key
                ORDER BY created_dt NULLS LAST
                ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS revolving_card_bucket,
        FIRST_VALUE(CASE WHEN last_dec.revolving_balance <= 3000 THEN 'A:<=3K'
                        WHEN last_dec.revolving_balance <= 6000 THEN 'B:<=6K'
                        WHEN last_dec.revolving_balance <= 10000 THEN 'C:<=10K'
                        WHEN last_dec.revolving_balance <= 20000 THEN 'D:<=20K'
                        WHEN last_dec.revolving_balance > 20000 THEN 'E:>20K'
                        END IGNORE NULLS)
            OVER (PARTITION BY a.user_key
                ORDER BY created_dt NULLS LAST
                ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS revolving_balance_bucket
    --joins/table references begin
    FROM
        user_management_pii.user_account a --base account table; contains info about a customer
        LEFT JOIN bc_pii.loc_account b --base consumer table
            ON a.user_key=b.user_key
        LEFT JOIN (
            SELECT DISTINCT
                loc_account_key,
                eligible,
                as_of,
                RANK() OVER (PARTITION BY loc_account_key ORDER BY id) AS dec_rnk
            FROM bc_pii.underwriting_history
        ) first_dec --sometimes a person will have two "decisions" during their application; first for eligibility and the latter for pricing
            ON b.loc_account_key = first_dec.loc_account_key
            AND first_dec.dec_rnk = 1
        LEFT JOIN (
            SELECT DISTINCT
                uh.loc_account_key,
                as_of,
                executed_on,
                eligible,
                is_loan_offer,
                decision_code,
                originator,
                CASE WHEN cdd.fico9_score BETWEEN 300 AND 850 THEN cdd.fico9_score END AS fico, --valid FICOs
                cdd.revolving_balance,
                RANK() OVER (PARTITION BY uh.loc_account_key ORDER BY executed_on DESC NULLS LAST, uh.id DESC) AS dec_rnk
            FROM bc_pii.underwriting_history uh
            LEFT JOIN bc_pii.loc_account_decisioning_detail cdd
                ON uh.loc_account_key = cdd.loc_account_key
        ) last_dec --sometimes a person will have two "decisions" during their application
            ON b.loc_account_key = last_dec.loc_account_key
            AND last_dec.dec_rnk = 1
        LEFT JOIN (
            SELECT DISTINCT
                uwh.loc_account_key,
                as_of,
                executed_on,
                eligible,
                is_loan_offer,
                decision_code,
                originator,
                CASE WHEN dec_det.fico9_score BETWEEN 300 AND 850 THEN dec_det.fico9_score END AS fico, --valid FICOs
                dec_det.revolving_balance,
                RANK() OVER (PARTITION BY uwh.loc_account_key ORDER BY executed_on DESC NULLS LAST, uwh.id DESC) AS RnW
            FROM bc_pii.underwriting_history uwh
            LEFT JOIN bc_pii.loc_account_decisioning_detail dec_det
                ON uwh.decisioning_detail_id = dec_det.id
        ) debt_track_dec
        ON b.loc_account_key = debt_track_dec.loc_account_key
        AND debt_track_dec.RnW = 2
        LEFT JOIN (
            SELECT *
            FROM temp.kpi_borrowers_settled --this is an analyst-managed table that tracks when people become borrowers
            WHERE month_num_asc = 0 --this table tracks borrower status monthly, 0 is the first month they appear on record
        ) borr --table constructed to identify who is a borrower/not
            ON b.id = borr.consumer_id
        LEFT JOIN (
            SELECT DISTINCT user_key,
                    MIN(a.created_on AT TIME ZONE 'America/Los_Angeles'::DATE) as card_input_dt
        FROM bc_pii.card_account a -- updated to pull from a bankcore table instead of segment
        WHERE a.customer_approval_status <> 'INIT'
        GROUP BY 1
        ) card_input --table that uses Segment screens to identify that someone has successfully added at least one cards
            ON card_input.user_key = b.user_key
        LEFT JOIN (
            SELECT DISTINCT
                user_id,
                FIRST_VALUE(platform) OVER (PARTITION BY user_id ORDER BY created_on ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS platform
            FROM user_management_pii.signup_user_agent_detail -- this table is a transformation of segment data. Probably best to go straight to the source
        ) user_platform --used to identify ios, android, or web
            ON user_platform.user_id = a.user_id
        LEFT JOIN (
            SELECT DISTINCT loc_account_key FROM bc_pii.loan_discount_agreement
        ) do_agreement --if a consumer_id is in this table, they're a discount offer customer
            ON do_agreement.loc_account_key = b.loc_account_key
        LEFT JOIN (
            SELECT DISTINCT
                loc_account_key,
                COUNT(DISTINCT CASE WHEN ob_pmt_strategy = 'REVOLVER' THEN card_id END) AS ob_revolver_ct
            FROM (
                SELECT DISTINCT
                    loc_acct.loc_account_key,
                    card.id AS card_id,
                    card.organization_id,
                    card.card_number,
                    cash.init_pmt_strategy,
                    card.initial_payment_strategy,
                    CASE WHEN cash.init_pmt_strategy = 'LINK_FAILED'
                            OR cash.first_summ < loc_acct.activated_on
                            THEN cash.init_pmt_strategy
                        ELSE card.initial_payment_strategy
                        END AS ob_pmt_strategy
                FROM
                    bc_pii.loc_account AS loc_acct --list of LOC accounts
                    LEFT JOIN bc_pii.card_account AS card --list of all cards added by consumers
                        ON card.user_key = loc_acct.user_key
                        AND (
                            card.created_on < loc_acct.activated_on --customers can add cards later in their lifecycle, so we're limiting it here to details of cards which they added during LOC onboarding
                            OR loc_acct.activated_on IS NULL
                            ) --still collect details for customers who add cards but don't activate
                    LEFT JOIN (
                        SELECT DISTINCT
                            card_id,
                            FIRST_VALUE(payment_strategy) OVER (PARTITION BY card_id ORDER BY created_on ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS init_pmt_strategy,
                            FIRST_VALUE(created_on) OVER (PARTITION BY card_id ORDER BY created_on ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS first_summ
                        FROM bc_pii.card_account_summary_history --contains history of cards added and their statuses over time
                    ) AS cash
                        ON cash.card_id = card.id
            ) AS ob_cards
            GROUP BY 1
        ) AS ob_card_summ --summary of cards added during onboarding experience
            ON ob_card_summ.loc_account_key = b.loc_account_key
        LEFT JOIN ( --Accounts created through affiliate API. These are pulled using the tables in the affiliates schema
            SELECT DISTINCT
                consumer_key,
                consumer_id,
                affiliate_vendor,
                created_at,
                application_dt,
                eligible_dt,
                card_input_dt,
                pw_reset_dt,
                seen_offer_dt,
                offer_dt,
                accept_dt,
                kyc_dt,
                activated_dt,
                borrower_dt
            FROM
                marketing.prequal_affiliate_funnel
        ) AS prequal_api
            ON a.user_key = prequal_api.consumer_key
	LEFT JOIN (
	    SELECT loc_account_key,
	           created_on,
	           activated_on,
	           first_eligibility,
	           grad_eligibility,
	           grad_offer_dt,
	           decision_code,
	           dt_accept_dt,
	           is_loan_offer
	    FROM new_debt_tracker_pop) debt_tracker_pop
	ON b.loc_account_key = debt_tracker_pop.loc_account_key
);

--COUNTS MATCH
select count(*), count(distinct consumer_id), count(created_dt), count(eligible_dt), count(accept_dt), count(activated_dt), count(borrower_dt)
from og_all_users; --3795072,3422369,3280586,644268,238826,361115,149866

select count(*), count(distinct loc_account_key), count(created_dt), count(eligible_dt), count(accept_dt), count(activated_dt), count(borrower_dt)
from new_all_users; --3795072,3422369,3280586,644268,238826,361115,149866


--Mismatch of 2-3 rows between DO and Legacy; Research suggests this may be a timing issue (all 3 rows from 8/17)
select pricing_version, count(offer_dt) from og_all_users group by 1 order by 1;
-- Discount Offer,77410
-- Legacy,162516
-- ,142371

select pricing_version, count(offer_dt) from new_all_users group by 1 order by 1;
-- Discount Offer,77408
-- Legacy,162518
-- ,142371

with compare_pricing_version as
(select a.user_key, a.consumer_id, a.pricing_version as og_pricing_version, b.loc_account_key, b.pricing_version as new_pricing_version
from og_all_users a left join new_all_users b on a.user_key = b.user_key)

select count(*) from compare_pricing_version
where nvl(og_pricing_version, 'Null') <> nvl(new_pricing_version,'Null'); --3 rows; all 3 from 8/17
